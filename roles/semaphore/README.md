# Semaphore UI
Installs Semaphore UI
you will need to create an admin user by stopping semaphore and by running the following command with your particulars substituted in.

```bash
sudo systemctl stop && sudo -u semaphore semaphore user add --config /home/semaphore/.config/semaphore/config.json --login admin --name admin --email semaphore@example.com --password admin --admin
```

If you want all of your users defined by SSO sign into ansible semaphore after running the playbook and run the following command

```bash
sudo -u semaphore semaphore user change-by-login --config /home/semaphore/.config/semaphore/config.json --login username --admin
```
You can set supply a custom configuration by defining the semaphore_config_path.

If the default configuration suits you should change the `semaphore_cookie_hash`, `semaphore_cookie_encryption`,`semaphore_access_key_encryption` variables

To Generate a sample config you can run the `semaphore setup` command while on a host with semaphore installed as the user semaphore will run as, and it will generate config based on the answer to your prompts that can be used by the `semaphore_config_path` variable. 
