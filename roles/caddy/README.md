# Caddy
Deploys the Caddy Reverse Proxy.

## Vars
- `caddy_template_path`: a template that generates a valid Caddy file.
